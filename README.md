# pkgweb

Code for generating a web interface to Portmod's package repositories.

Currently this relies on the development version of portmod (and indeed the unmerged feature branch in portmod!334).

## Dependencies

- Python, portmod and jinja2 (https://pypi.org/project/Jinja2/)
- Soupault: https://soupault.app/#downloads
- Stork: https://stork-search.net/docs/install

For convenience, the `output.sh` script will download missing dependencies automatically (for python dependencies, this requires virtualenv).

## Building

Run `output.sh`. The resulting site will be created in a directory called `build`.
