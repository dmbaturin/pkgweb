# Copyright 2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""
Creates pages for packages
"""

import json
import os
import sys
from typing import Dict, List

from jinja2 import Template
from portmod.globals import env
from portmod.loader import load_all
from portmod.parsers.usestr import use_reduce
from portmod.pybuild import Pybuild
from portmod.query import get_flags, get_maintainer_string
from portmod.repo import Repo, get_repo_name
from portmod.repo.metadata import get_archs, get_package_metadata
from portmod.util import get_newest

root = os.path.abspath(sys.argv[1])
# FIXME: Loading packages shouldn't require state
env.REPOS.append(Repo(location=root, name=get_repo_name(root)))
gitlab_url = sys.argv[2]

# FIXME: insert repo defined at root
grouped: Dict[str, List[Pybuild]] = {}

for pkg in load_all(only_repo_root=root):
    if pkg.CPN in grouped:
        grouped[pkg.CPN].append(pkg)
    else:
        grouped[pkg.CPN] = [pkg]

archs = sorted(get_archs(root))

template = Template(
    """
<ll>
  <li><a href="{{ issues }}">Issues</a></li>
  <li><a href="{{ mrs }}">Merge Requests</a></li>
  <li><a href="{{ tree }}">Tree</a></li>
  <li><a href="{{ history }}">History</a></li>
<ll>

<main>
    <h2>{{ atom }}</h2>
    <h1>{{ name }}</h1>
    <p>{{ desc }}</p>
{% if homepage is defined %}
    <p><a href="{{ homepage }}">{{ homepage }}</a></p>
{% endif %}
<table>
    <tr>
        <td>Version</td>
        {% for arch in archs %}
        <td>{{ arch }}</td>
        {% endfor %}
    </tr>
{% for version in versions %}
  <tr>
      <td><a href="{{ tree }}/{{ pn }}-{{ version }}.pybuild">{{ version }}</a></td>
      {% for keyword in keywords %}
            <td>{{ keyword}}</td>
      {% endfor %}
  </tr>
{% endfor %}
</table>

<table>
{% if longdescription is defined %}
<tr>
    <td>Long description</td>
    <td>{{ longdescription }}</td>
</tr>
{% endif %}

{% if other_homepages is defined %}
<tr>
    <td>Other Homepages</td>
    <td>
      {% for url in other_homepages %}
        <p><a href="{{ url }}">{{ url }}</a></p>
      {% endfor %}
    </td>
</tr>
{% endif %}
<tr>
    <td>USE flags</td>
    <td>
{% if local_use is defined %}
    <h4>Local USE flags</h4>
    <table>
  {% for flag in local_use %}
    <tr>
        <td>{{ flag }}</td>
        <td>{{ local_use[flag] }}</td>
    </p>
  {% endfor %}
    </table>
{% endif %}
{% if global_use is defined %}
    <h4>Global USE flags</h4>
    <table>
  {% for flag in global_use %}
    <tr>
        <td>{{ flag }}</td>
        <td>{{ global_use[flag] }}</td>
    </tr>
  {% endfor %}
    </table>
{% endif %}
{% if use_expand is defined %}
  {% for typ in use_expand %}
    <h4>USE_EXPAND flags ({{ typ }})</h4>
    <table>
  {% for flag in use_expand[typ] %}
    <tr>
        <td>{{ flag }}</td>
        <td>{{ use_expand[typ][flag] }}</td>
    </tr>
  {% endfor %}
    </table>
  {% endfor %}
{% endif %}
</tr>
{% if license is defined %}
<tr>
    <td>License</td>
    <td>{{ license }}</td>
</tr>
{% endif %}
{% if maintainer is defined %}
<tr>
    <td>Maintainer</td>
    <td>{{ maintainer }}</td>
{% endif %}
{% if upstream is defined %}
<tr>
    <td>Upstream</td>
    <td>
    <table>
        {% if upstream.get("maintainer") != None %}
        <tr>
            <td>Author/Maintainer</td>
            <td>{{ upstream["maintainer"] }}</td>
        </tr>
        {% endif %}
        {% if upstream.get("bugs-to") != None %}
        <tr>
            <td>Bugs To</td>
            <td>{{ upstream["bugs-to"] }}</td>
        </tr>
        {% endif %}
        {% if upstream.get("doc") != None %}
        <tr>
            <td>Documentation</td>
            <td>{{ upstream["doc"] }}</td>
        </tr>
        {% endif %}
        {% if upstream.get("changelog") != None %}
        <tr>
            <td>Changelog</td>
            <td>{{ upstream["changelog"] }}</td>
        </tr>
        {% endif %}
    </table>
    </td>
</tr>
{% endif %}
</table>
</main>
"""
)

output = "site"
os.makedirs(output, exist_ok=True)

with open("stork.toml", "w") as file:
    print("[output]", file=file)
    # Note: the index goes in the build directory, since soupault won't process it
    print('filename = "build/index.st"', file=file)
    print("[input]", file=file)
    print(f'base_directory = "{output}"', file=file)

for group, pkgs in grouped.items():
    # Collect version and keyword for each package version
    # Use name, desc and homepage from most recent version
    # Collect package metadata
    # Generate link for searching the bug tracker
    versions = []
    for pkg in pkgs:
        versions.append(pkg.ATOM.PVR)
    keywords = []
    for arch in archs:
        if arch in pkg.KEYWORDS:
            keywords.append("stable")
        elif "~" + arch in pkg.KEYWORDS:
            keywords.append("unstable")
        elif "-" + arch in pkg.KEYWORDS:
            keywords.append("unsupported")
        else:
            keywords.append("")
    newest = get_newest(pkgs)
    metadata = get_package_metadata(newest)

    homepages = use_reduce(newest.HOMEPAGE, matchall=True, flat=True)

    local_use = {}
    global_use = {}
    use_expand = {}
    for pkg in pkgs:
        loc, glob, exp = get_flags(pkg)
        local_use.update(loc)
        global_use.update(glob)
        use_expand.update(exp)

    optionals = {}

    # TODO: Use flags should link to page showing all packages using that flag
    if local_use:
        optionals["local_use"] = local_use
    if global_use:
        optionals["global_use"] = global_use
    if use_expand:
        optionals["use_expand"] = use_expand

    if homepages:
        optionals["homepage"] = homepages[0]
    if len(homepages) > 1:
        optionals["other_homepages"] = homepages[1:]

    # FIXME: Should include a link to the license in tree
    if newest.LICENSE:
        optionals["license"] = newest.LICENSE

    if metadata:
        if metadata.longdescription:
            optionals["longdescription"] = metadata.longdescription
        if metadata.maintainer:
            optionals["maintainer"] = get_maintainer_string(metadata.maintainer)
        if metadata.upstream:
            optionals["upstream"] = {}
            if metadata.upstream.maintainer:
                optionals["upstream"]["maintainer"] = get_maintainer_string(
                    metadata.upstream.maintainer
                )
            if metadata.upstream.doc:
                optionals["upstream"]["doc"] = metadata.upstream.doc
            if metadata.upstream.bugs_to:
                optionals["upstream"]["bugs-to"] = metadata.upstream.bugs_to
            if metadata.upstream.changelog:
                optionals["upstream"]["changelog"] = metadata.upstream.changelog

    html = template.render(
        pn=newest.PN,
        atom=group,
        name=newest.NAME,
        desc=newest.DESC,
        issues=f"{gitlab_url}/-/issues?scope=all&search={group}",
        mrs=f"{gitlab_url}/-/merge_requests?scope=all&search={group}",
        tree=f"{gitlab_url}/-/tree/master/{group}",
        history=f"{gitlab_url}/-/commits/master/{group}",
        versions=versions,
        keywords=keywords,
        archs=archs,
        **optionals,
    )

    os.makedirs(os.path.dirname(os.path.join(output, group)), exist_ok=True)
    with open(os.path.join(output, group + ".html"), "w") as file:
        file.write(html)

    with open("stork.toml", "a") as file:
        print("[[input.files]]", file=file)
        print(f'path = "{group}.html"', file=file)
        print(f'url = "{group}.html"', file=file)
        print(f'title = "{newest.NAME}"', file=file)
