#!/bin/bash

# Dependencies are:
# stork: https://stork-search.net/docs/install
# soupault: https://soupault.app/#downloads
# python3+jinja2: https://pypi.org/project/Jinja2/

if [[ $# != 4 ]]; then
    echo "Usage $0 <repo_path> <gitlab_url> <site_title> <site_root_url>"
    exit
fi

repo=$1
site=$2
name=$3
export SITE_ROOT=$4

python -c "import jinja2" && python -c "import portmod"
if [ $? -ne 0 ]; then
    virtualenv .venv
    source .venv/bin/activate
    pip install --upgrade jinja2 portmod
fi
python packages.py $repo $site || exit 1
echo "<h1>$name</h1>" > site/index.html

if [[ ! -r "site/stork.js" ]]; then
    wget https://github.com/jameslittle230/stork/releases/download/v1.1.0/stork.js -O site/stork.js
    wget https://github.com/jameslittle230/stork/releases/download/v1.1.0/stork.wasm -O site/stork.wasm
fi
if [[ ! -r soupault && ! `which soupault` ]]; then
    ver=2.4.0
    wget https://files.baturin.org/software/soupault/${ver}/soupault-${ver}-linux-x86_64.tar.gz
    tar -xzf soupault-${ver}-linux-x86_64.tar.gz
    mv soupault-${ver}-linux-x86_64/soupault .
    rm -r soupault-${ver}-linux-x86_64
    rm soupault-${ver}-linux-x86_64.tar.gz
    chmod +x soupault
fi
soupault=`which soupault || echo ./soupault`
$soupault || exit 1

if [[ ! -r stork && ! `which stork` ]]; then
    wget https://files.stork-search.net/releases/latest/stork-ubuntu-latest -O stork
    chmod +x stork
fi
stork=`which stork || echo ./stork`
$stork --build stork.toml || exit 1
